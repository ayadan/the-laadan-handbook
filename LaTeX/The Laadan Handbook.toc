\contentsline {chapter}{\numberline {1}Introduction}{3}
\contentsline {section}{\numberline {1.1}What is L\IeC {\'a}adan?}{3}
\contentsline {section}{\numberline {1.2}Contributors}{5}
\contentsline {section}{\numberline {1.3}This is a Free book}{5}
\contentsline {chapter}{\numberline {2}Grammar}{6}
\contentsline {section}{\numberline {2.1}Lesson 1: Sounds, tones, and euphony}{6}
\contentsline {paragraph}{Examples of transliterating names:}{8}
\contentsline {paragraph}{Examples of translating name meanings:}{9}
\contentsline {paragraph}{Example dialog:}{9}
\contentsline {section}{\numberline {2.2}Lesson 2: It is \relax $\@@underline {\hbox {\hspace {3cm}}}\mathsurround \z@ $\relax }{10}
\contentsline {paragraph}{Examples of \textit {Verb Case-Phrase-Subject} sentence structure:}{11}
\contentsline {section}{\numberline {2.3}Lesson 3: Verb negation}{12}
\contentsline {section}{\numberline {2.4}Lesson 4: The Speech Act Morpheme and the Evidence Morpheme}{13}
\contentsline {paragraph}{Examples of using each Speech Act Morpheme:}{13}
\contentsline {paragraph}{Examples of using each Evidence Morpheme:}{14}
\contentsline {paragraph}{Vocabulary}{15}
\contentsline {section}{\numberline {2.5}Lesson 5: Tense}{16}
\contentsline {section}{\numberline {2.6}Lesson 6: Adjectives}{16}
\contentsline {section}{\numberline {2.7}Lesson 7: Plurals}{16}
\contentsline {section}{\numberline {2.8}Lesson 8: Pronouns}{16}
\contentsline {section}{\numberline {2.9}Lesson 9: Object marker}{16}
\contentsline {section}{\numberline {2.10}Lesson 10: Multiple verbs}{16}
\contentsline {section}{\numberline {2.11}Lesson 11: Trying}{16}
\contentsline {section}{\numberline {2.12}Lesson 12: The Source marker (from) and the Goal marker (to)}{16}
\contentsline {section}{\numberline {2.13}Lesson 13: The Path amrker (through, across)}{16}
\contentsline {section}{\numberline {2.14}Lesson 14: The Association marker (with)}{16}
\contentsline {section}{\numberline {2.15}Lesson 15: The Beneficiary marker (for)}{16}
\contentsline {section}{\numberline {2.16}Lesson 16: The Instrument marker (per/by means of)}{16}
\contentsline {section}{\numberline {2.17}Lesson 17: The Location marker (at)}{16}
\contentsline {section}{\numberline {2.18}Lesson 18: The Manner marker (how?)}{16}
\contentsline {section}{\numberline {2.19}Lesson 19: The Reason marker (why?)}{16}
\contentsline {section}{\numberline {2.20}Lesson 20: The Cause marker (in order to...)}{16}
\contentsline {section}{\numberline {2.21}Lesson 21: The Possession markers}{16}
\contentsline {section}{\numberline {2.22}Lesson 22: The Degree markers}{16}
\contentsline {section}{\numberline {2.23}Lesson 23: The Duration markers}{16}
\contentsline {section}{\numberline {2.24}Lesson 24: The Repetition markers}{16}
\contentsline {section}{\numberline {2.25}Lesson 25: Ways to perceive}{16}
\contentsline {section}{\numberline {2.26}Lesson 26: Interpersonal relationships}{16}
\contentsline {section}{\numberline {2.27}Lesson 27: Speech Act Morpheme additions}{16}
\contentsline {section}{\numberline {2.28}Lesson 28: Noun declensions}{16}
\contentsline {section}{\numberline {2.29}Lesson 29: Comparisons}{16}
\contentsline {section}{\numberline {2.30}Lesson 30: Who, what, when, where, why?}{16}
\contentsline {section}{\numberline {2.31}Lesson 31: Passive voice}{16}
\contentsline {section}{\numberline {2.32}Lesson 32: Embedded sentences}{16}
\contentsline {section}{\numberline {2.33}Lesson 33: If... Then... sentences}{16}
\contentsline {section}{\numberline {2.34}Lesson 34: Interpersonal}{16}
\contentsline {chapter}{\numberline {3}Dictionary}{17}
\contentsline {chapter}{\numberline {4}Tips and Tricks for using L\IeC {\'a}adan}{18}
\contentsline {chapter}{\numberline {5}Entertainment}{19}
\contentsline {section}{\numberline {5.1}Stories}{19}
\contentsline {section}{\numberline {5.2}Comics}{19}
\contentsline {section}{\numberline {5.3}Sheet music}{19}
\contentsline {chapter}{\numberline {6}Word Lists}{20}
\contentsline {chapter}{\numberline {7}Phrase Book}{24}
\contentsline {chapter}{\numberline {8}Quick Reference}{25}
